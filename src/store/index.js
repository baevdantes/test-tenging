import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import { api } from 'src/api'

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  return createStore({
    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    state: {
      stores: []
    },
    mutations: {
      setStores (state, payload) {
        state.stores = payload
      }
    },
    actions: {
      getStores ({ commit }) {
        return api.getStores().then((res) => {
          commit('setStores', res.data)
        })
      }
    },
    strict: process.env.DEBUGGING
  })
})

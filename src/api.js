import axios from 'axios'

export const api = {
  getStores () {
    return axios.get('https://demoapi.thedenstore.com/api/service?Request=Stores&Language=en-us')
  }
}
